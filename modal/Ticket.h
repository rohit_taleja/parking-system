using namespace std;
class Ticket{	
    char *vehicalType;
  	int slotNo;
  	int ticketNo;
  	char *entryTime;
  	time_t startTime;
  public:
  	Ticket(int ticketNo,string entryTime,string vehicalType,int slotNo){
  		time(&this->startTime);
  		this->entryTime=new char[entryTime.length()];
  		this->vehicalType=new char[vehicalType.length()];
  		this->ticketNo=ticketNo;
  	    this->slotNo=slotNo;
		strcpy(this->vehicalType, vehicalType.c_str());
  		strcpy(this->entryTime, entryTime.c_str());
	  }
	  
	  char *getEntryTime(){
	  	return this->entryTime;
	  }
	  char *getVehicleType(){
	  	return this->vehicalType;
	  }	
	  int getTicketNo(){
	  	return ticketNo;
	  }
	  int getSlotNo(){
	  	return slotNo;
	  } 	  
	  time_t getStartTime(){
	  	return this->startTime;
	  }
};

