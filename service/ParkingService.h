using namespace std;
class ParkingService {
private:
    int getTicketId(string vehicleType, int slotNo)
    {
        int ticketId = ++ticketGentator;
        if (vehicleType == TWO_VHILAR)
            ticketId = ticketId * 10 + 2;
        else
            ticketId = ticketId * 10 + 4;
        ticketId = ticketId * 100 + slotNo;
    }

    Ticket* createTicketObj(string vehicleType, int slotNo)
    {
        return new Ticket(getTicketId(vehicleType, slotNo), getCurrentDateNdTime(), vehicleType, slotNo);
    }

    int getSlotNo(int ticketNo)
    {
        return ticketNo % 100;
    }

    int getVehicalType(int ticketNo)
    {
        return (ticketNo / 100) % 10;
    }

    bool isTicketNoEqual(int ticketNo1, int ticketNo2)
    {
        if (ticketNo1 == ticketNo2)
            return true;
        return false;
    }

    double calculateParkingRent(time_t startTime, float pricePerHour)
    {
        time_t endTime;
        time(&endTime);
        double seconds = difftime(endTime, startTime);
        return (pricePerHour * ceil((double)seconds / 3600));
    }

    bool isVehicleNoEqual(string vehicleNo1, string vehicleNo2)
    {
        if (vehicleNo2 == vehicleNo1)
            return true;
        return false;
    }

    bool isTwoVehilarExist(Parking* parking, string vehicleNo)
    {
        list<TwoVehilar*>::iterator it;
        TwoVehilar* twoVehilar;
        for (int slotNo = 0; slotNo < NO_OF_SLOTS; slotNo++) {
            it = parking->slots[slotNo]->twoVehilarArea->area.begin();
            while (it != parking->slots[slotNo]->twoVehilarArea->area.end()) {
                twoVehilar = *it;
                it++;
                if (isVehicleNoEqual(twoVehilar->getVNo(), vehicleNo))
                    return true;
            }
        }
        return false;
    }

    bool isFourVehilarExist(Parking* parking, string vehicleNo)
    {
        list<FourVehilar*>::iterator it;
        FourVehilar* fourVehilar;
        for (int slotNo = 0; slotNo < NO_OF_SLOTS; slotNo++) {
            it = parking->slots[slotNo]->fourVehilarArea->area.begin();
            while (it != parking->slots[slotNo]->fourVehilarArea->area.end()) {
                fourVehilar = *it;
                it++;
                if (isVehicleNoEqual(fourVehilar->getVNo(), vehicleNo))
                    return true;
            }
        }
        return false;
    }

    bool isVehicleExist(Parking* parking, string vehicleNo)
    {
        if (isTwoVehilarExist(parking, vehicleNo))
            return true;

        if (isFourVehilarExist(parking, vehicleNo))
            return true;

        return false;
    }

    bool deparkTwoVehilar(Parking* parking, int slotNo, int ticketNo)
    {

        list<TwoVehilar*>::iterator it = parking->slots[slotNo]->twoVehilarArea->area.begin();
        TwoVehilar* twoVehilar;
        while (it != parking->slots[slotNo]->twoVehilarArea->area.end()) {
            twoVehilar = *it;
            it++;
            if (isTicketNoEqual(twoVehilar->ticket->getTicketNo(), ticketNo)) {
                cout << "\n\t\tyou rent is: " << calculateParkingRent(twoVehilar->ticket->getStartTime(), TWO_VEHICAL_PRICE_PER_HOUR) << endl
                     << endl;
                parking->slots[slotNo]->twoVehilarArea->area.remove(twoVehilar);
                return true;
            }
        }
    }

    bool deparkFourVehilar(Parking* parking, int slotNo, int ticketNo)
    {

        list<FourVehilar*>::iterator it = parking->slots[slotNo]->fourVehilarArea->area.begin();
        FourVehilar* fourVehilar;
        while (it != parking->slots[slotNo]->fourVehilarArea->area.end()) {
            fourVehilar = *it;
            it++;
            if (isTicketNoEqual(fourVehilar->ticket->getTicketNo(), ticketNo)) {
                cout << "\n\t\tyou rent is: " << calculateParkingRent(fourVehilar->ticket->getStartTime(), FOUR_VEHICAL_PRICE_PER_HOUR) << endl
                     << endl;
                parking->slots[slotNo]->fourVehilarArea->area.remove(fourVehilar);
                return true;
            }
        }
        return false;
    }

public:
    void parkTwoVhilar(Parking* parking, string vehicleNo)
    {
    	
        for (int i = 0; i < NO_OF_SLOTS; i++) {
            if (parking->slots[i]->twoVehilarArea->area.size() < areaSize(i, TWO_VHILAR)) {
                TwoVehilar* twoVehilar = new TwoVehilar(vehicleNo, createTicketObj(TWO_VHILAR, i));
                parking->slots[i]->twoVehilarArea->area.push_back(twoVehilar);
                cout << endl;
                TwoVehilarService twoVehilarService;
                twoVehilarService.printTicket(twoVehilar);
                return;
            }
        }
        ParkingAreaFULL parkingAreaFULL("\n\t-----------Two Vehilar Parking Space FULL-----------\n");
        throw parkingAreaFULL;
    }

    void parkFourVhilar(Parking* parking, string vehicleNo)
    {

        for (int i = 0; i < NO_OF_SLOTS; i++) {
            if (parking->slots[i]->fourVehilarArea->area.size() < areaSize(i, FOUR_VHILAR)) {
                FourVehilar* fourVehilar = new FourVehilar(vehicleNo, createTicketObj(FOUR_VHILAR, i));
                parking->slots[i]->fourVehilarArea->area.push_back(fourVehilar);
                FourVehilarService fourVehilarService;
                cout << endl;
                fourVehilarService.printTicket(fourVehilar);
                return;
            }
        }
        ParkingAreaFULL parkingAreaFULL("\n\t-----------Four Vehilar Parking Space FULL-----------\n");
        throw parkingAreaFULL;
    }

    void parkVehilcle(Parking* parking, string vehicleNo,string vehicleType){
    	
    	if (isVehicleExist(parking, vehicleNo)) {
            VehicleAlreayParked vehicleAlreayParked("\n\t---------- Vehicle no. has benn parked alread ----------\n");
            throw vehicleAlreayParked;
        }
        if(vehicleType==TWO_VHILAR){
        	parkTwoVhilar(parking,vehicleNo);
		}else if(vehicleType==FOUR_VHILAR){
			parkFourVhilar(parking,vehicleNo);
		}else{
			 InvalidVehicleType invalidVehicleType("\n\t---------- Vehicle type is Invalid -----------\n");
			 throw invalidVehicleType;
		}
        
	}
    void deparkVehicle(Parking* parking, int ticketNo)
    {
        int slotNo = getSlotNo(ticketNo);
        int isVehical = getVehicalType(ticketNo);
        if (isVehical == 2) {
            if (deparkTwoVehilar(parking, slotNo, ticketNo))
                return;
        }
        else if (isVehical == 4) {

            if (deparkFourVehilar(parking, slotNo, ticketNo))
                return;
        }
        InvalidTicket invalidTicket;
        throw invalidTicket;
    }

    void freeParkingSpace(Parking* parking)
    {
        cout << endl;
        cout << "Slot 1. twoV capacity  " << areaSize(0, TWO_VHILAR) - parking->slots[0]->twoVehilarArea->area.size() << endl;
        cout << "Slot 1. fourV capacity " << areaSize(0, FOUR_VHILAR) - parking->slots[0]->fourVehilarArea->area.size() << endl;
        cout << "Slot 2. twoV capacity  " << areaSize(1, TWO_VHILAR) - parking->slots[1]->twoVehilarArea->area.size() << endl;
        cout << "Slot 2. fourV capacity " << areaSize(1, FOUR_VHILAR) - parking->slots[1]->fourVehilarArea->area.size() << endl;
        cout << "Slot 3. twoV capacity  " << areaSize(2, TWO_VHILAR) - parking->slots[2]->twoVehilarArea->area.size() << endl;
        cout << "Slot 3. fourV capacity " << areaSize(2, FOUR_VHILAR) - parking->slots[2]->fourVehilarArea->area.size() << endl;
        cout << endl;
    }
};
