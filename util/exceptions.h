using namespace std; 
class TwoVehilarParkingAreaFULL: public exception
{
	public:
  virtual const char* what() const throw()
  {
    return "\n\t-----------Two Vehilar Parking Space FULL-----------\n";
  }
}; 


class FourVehilarParkingAreaFULL: public exception
{
	public:
  virtual const char* what() const throw()
  {
    return "\n\t-----------Four Vehilar Parking Space FULL-----------\n";
  }
}; 

class ParkingAreaFULL: public exception {
private:
    string message_;
public:
    ParkingAreaFULL(const std::string& message) : message_(message) {
    }
    ~ParkingAreaFULL() throw(){};
	virtual const char* what() const throw() {
        return message_.c_str();
    }    
};

class VehicleAlreayParked: public exception {
private:
    string message_;
public:
    VehicleAlreayParked(const std::string& message) : message_(message) {
    }
    ~VehicleAlreayParked() throw(){};
	virtual const char* what() const throw() {
        return message_.c_str();
    }    
};

class InvalidTicket: public exception
{
	public:
  virtual const char* what() const throw()
  {
    return "\n\t-----------Ticket is invalid-----------\n";
  }
}; 


class InValidDataType: public exception {
private:
    string message_;
public:
    InValidDataType(const std::string& message) : message_(message) 
{
}
    ~InValidDataType() throw(){};
	virtual const char* what() const throw() {
        return message_.c_str();
    }    
};

class InvalidVehicleType: public exception {
private:
    string message_;
public:
    InvalidVehicleType(const std::string& message) : message_(message) 
{
}
    ~InvalidVehicleType() throw(){};
	virtual const char* what() const throw() {
        return message_.c_str();
    }    
};







