#include <bits/stdc++.h>
#include "F:\com\util\constant.h"
#include "F:\com\util\exceptions.h"
#include "F:\com\util\methods.h"
#include "F:\com\modal\Ticket.h"
#include "F:\com\modal\Vehicle.h"
#include "F:\com\modal\TwoVehilar.h"
#include "F:\com\modal\fourVehilar.h"
#include "F:\com\modal\TwoVehilarParkingArea.h"
#include "F:\com\modal\FourVehilarParkingArea.h"
#include "F:\com\modal\ParkingSlot.h"
#include "F:\com\modal\parking.h"
#include "F:\com\service\ticketService.h"
#include "F:\com\service\twoVehilarService.h"
#include "F:\com\service\fourVehilarService.h"
#include "F:\com\service\parkingservice.h"
using namespace std;
int main()
{
    Parking parking;
    ParkingService parkingService;
    string vehicleNo;
    string vehicleType;
    int option;
    int ticketNo;
    while (1) {
        cout << "[Park press-> 1]\t"
             << "[DePark press-> 2]\t"
             << "[VacantSpace press-> 3]" << endl;

        try {
            option = readInteger();
        }
        catch (InValidDataType& e) {
            cout << e.what();
            continue;
        }
        fflush(stdin);

        switch (option) {
        case 1:
            cout << "enter vehicle no.    ";
            cin >> vehicleNo;
            cout << "enter vehicle type.  ";
            fflush(stdin);
            cin >> vehicleType;
            fflush(stdin);
            if (vehicleType == TWO_VHILAR) {
                try {
                    parkingService.parkTwoVhilar(&parking, vehicleNo);
                }
                catch (VehicleAlreayParked& e) {
                    cout << e.what();
                }
                catch (ParkingAreaFULL& e) {
                    cout << e.what();
                }
            }
            else if (vehicleType == FOUR_VHILAR) {
                try {
                    parkingService.parkFourVhilar(&parking, vehicleNo);
                }
                catch (VehicleAlreayParked& e) {
                    cout << e.what();
                }
                catch (ParkingAreaFULL& e) {
                    cout << e.what();
                }
            }
            else
                cout << "\n\t-------Invalid vehical type-------- " << endl;
            break;
        case 2:
            cout << "enter Ticket no ";
            try {
                ticketNo = readInteger();
                parkingService.deparkVehicle(&parking, ticketNo);
            }
            catch (InValidDataType& e) {
                cout << e.what();
            }
            catch (InvalidTicket& e) {
                cout << e.what();
            }
            break;
        case 3:
            parkingService.freeParkingSpace(&parking);
            break;
        default:
            cout << "\n\t -----worng choice------\n";
        }
    }
    return 0;
}
